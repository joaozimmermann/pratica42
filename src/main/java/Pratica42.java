
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Joao
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse teste = new Elipse(2, 4);
        Circulo teste2 = new Circulo(2);
        System.out.println(teste.getPerimetro());
        System.out.println(teste.getArea());
        System.out.println(teste2.getPerimetro());
    }
}
